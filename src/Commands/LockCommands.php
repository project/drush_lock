<?php

namespace Drupal\drush_lock\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for handling locks in concurrently executed scripts.
 */
class LockCommands extends DrushCommands {

  const GLOBAL_LOCK = 'drush_lock';

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * A key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * LockCommands constructor.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key-value factory.
   */
  public function __construct(LockBackendInterface $lock, KeyValueFactoryInterface $keyValueFactory) {
    $this->lock = $lock;
    $this->keyValue = $keyValueFactory->get('drush_lock');
  }

  /**
   * Wait for (and acquire) a lock.
   *
   * Acquires, and if it needs to, waits for the named lock. Will return once
   * the lock has been acquired. IMPORTANT: call lock:release to release the
   * lock again.
   *
   * @param string $lock
   *   The name of the lock to acquire. Note that this does not correspond to
   *   a Drupal lock.
   *
   * @option delay
   *   The amount of seconds to wait for the lock.
   *
   * @usage lock:wait my_lock --wait=60
   *   Wait for and acquire the my_lock. Wait for a maximum of 60 seconds.
   *
   * @command lock:wait
   *
   * @aliases lwait
   */
  public function lockWait($lock, $options = ['delay' => 30]) {
    // The lock we are acquiring does not correspond 1:1 to a Drupal lock,
    // since Drupal locking works on a PHP process basis. We'll use
    // pseudo locks we store in the key value store. We protect access to these
    // pseudo locks through the actual Drupal locking framework.
    $acquired = FALSE;

    // Wait at least a second.
    $delay = max(intval($options['delay']), 1);
    // Prime the sleep delay.
    $sleep = 0;

    while (!$acquired) {
      if ($this->lock->acquire(self::GLOBAL_LOCK)) {
        $locked = $this->keyValue->get($lock, FALSE);

        if (!$locked) {
          $this->keyValue->set($lock, TRUE);
          $acquired = TRUE;
        }

        $this->lock->release(self::GLOBAL_LOCK);
      }

      if (!$acquired && $delay != 0) {
        // Wait a random time between 1 and 5 seconds to try and acquire the
        // lock again (doing something to minimize the chance of two scripts
        // trying to get the lock at the same time). Allow the sleep delay to
        // build up by picking the max between a new random number and the
        // previous one. Once we reach 5 seconds, we'll wait 10 seconds between
        // iterations.
        $this->logger()->notice(dt('Waiting %delay more seconds to acquire lock %lock.', ['%delay' => $delay, '%lock' => $lock]));
        $sleep = max($sleep, rand(1, 5));

        // Make sure sleep is never more than the actual delay, for small delay
        // values.
        $sleep = min($sleep, $delay);

        // Cap off the delay at 0, so we don't go negative.
        $delay = max($delay - $sleep, 0);

        sleep($sleep);

        // When we have waited 5 seconds, let's increase to 10.
        if ($sleep == 5) {
          $sleep = 10;
        }
      }
      elseif ($delay == 0) {
        // If we reached delay 0 and end up here, we need to exit the loop.
        break;
      }
    }

    if ($acquired) {
      $this->logger()->success(dt('Acquired lock %lock', ['%lock' => $lock]));

      return CommandResult::exitCode(self::EXIT_SUCCESS);
    }
    else {
      $this->logger()->error(dt('Failed to acquire lock %lock', ['%lock' => $lock]));

      return CommandResult::exitCode(self::EXIT_FAILURE);
    }
  }

  /**
   * Release a lock previously acquired with lock:wait.
   *
   * Note that there is no mechanism to ensure the lock was actually acquired by
   * the current or any other script. This means it can also be used for
   * troubleshooting, e.g. when a script has not released a lock.
   *
   * @param string $lock
   *   The name of the lock to release. Note that this does not correspond to
   *   a Drupal lock.
   *
   * @usage lock:release my_lock
   *   Release the lock 'my_lock'.
   *
   * @command lock:release
   *
   * @aliases lrelease, lrel
   */
  public function lockRelease($lock) {
    // The lock we are acquiring does not correspond 1:1 to a Drupal lock,
    // since Drupal locking works on a PHP process basis. We'll use
    // pseudo locks we store in the key value store. We protect access to these
    // pseudo locks through the actual Drupal locking framework.
    if (!$this->lock->acquire(self::GLOBAL_LOCK)) {
      // We couldn't get the lock to get in our lock store. Wait some more.
      $wait = $this->lock->wait(self::GLOBAL_LOCK);

      if ($wait) {
        // We need to wait even longer, but we won't. Looks like something went
        // bad, as we don't typically retain the lock this long.
        $this->logger()->error(dt('Could not acquire access to lock store. Lock %lock was not released.', ['%lock' => $lock]));

        return CommandResult::exitCode(self::EXIT_FAILURE);
      }
    }

    // We will have the lock at this point.
    // Check if the lock was actually set to give a warning if that makes sense.
    $locked = $this->keyValue->get($lock, FALSE);
    $this->keyValue->delete($lock);

    $this->lock->release(self::GLOBAL_LOCK);

    if ($locked) {
      $this->logger()->success(dt('Released lock %lock', ['%lock' => $lock]));
    }
    else {
      $this->logger()->warning(dt('There was no lock named %lock', ['%lock' => $lock]));
    }

    return CommandResult::exitCode(self::EXIT_SUCCESS);
  }

}
