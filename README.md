Drush Lock
==========
Drush commands for handling locks. Allows protecting scripts from harmful 
concurrent execution.

Introduction
------------
Drush is a popular tool for controlling Drupal installations from the command
line and is also often included in (shell) scripts interacting with a Drupal
installation. In some situations, you might get in a situation where the 
same script could run multiple times, at roughly the same time, where that is
undesirable. The use case that led to the creation of this module is the 
starting of multiple replicas of a Drupal containter on Kubernetes, where the
container will automatically apply updates and import configuration on startup.

The commands in this module allow the script to wait for a named 'lock', much 
like the Locking API in Drupal does (note that the locks do not correspond
to actual Drupal locks, because Drupal locks should be acquired and released 
from a single PHP process).

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/drush_lock

Requirements
------------
This module relies on Drush and Drupal core. If you did not have those already,
you probably don't need this module.

Installation
------------
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
   
Configuration
-------------
This module does not require any configuration.

Commands
--------
For more information about these commands, use `drush help`.

### lock:wait
Wait for and acquire a named lock, e.g. `drush lock:wait my_lock --delay=60`; 
wait for the lock named *my_lock*, for a maximum of 60 seconds.

### lock:release
Release the named lock, e.g. `drush lock:release my_lock`.

Example
-------
Let's say you have a Drupal installation running on Kubernetes. You arranged 
things in such a way that the Drupal image will automatically detect that it 
needs to apply updates and import configuration when starting. However, if you 
run multiple replicas of your image, you may run into trouble. In general, your
various replicas may all try to apply updates, import configuration, etc. Even
if those individual steps may be protected against concurrent access, the 
sequence of the various steps is important too; in general, database updates 
should be applied before config import, for example. Enter drush_lock.

With drush_lock, you can add `drush lock:wait my_deployment_lock --delay 1800` 
before your update script, and `drush lock:release my_deployment_lock` after. 
This will ensure only one replica at a time will enter the critical section. The
1800 stands for half an hour, which should be more than enough for most Drupal
update processes to complete.

Maintainers
-----------
Current maintainers:

* Eelke Blok (eelkeblok) - https://www.drupal.org/u/eelkeblok
